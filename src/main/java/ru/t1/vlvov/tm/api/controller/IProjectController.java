package ru.t1.vlvov.tm.api.controller;

import ru.t1.vlvov.tm.model.Project;

public interface IProjectController {

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void createProject();

    void clearProjects();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
