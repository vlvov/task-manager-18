package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.model.User;
import java.util.Comparator;
import java.util.List;

public interface IUserRepository {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

}
